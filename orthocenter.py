#!/usr/bin/python3
import math
from sympy import Symbol
from sympy.solvers import solve

Xa = int(input("Xa? "))
Ya = int(input("Ya? "))
Xb = int(input("Xb? "))
Yb = int(input("Yb? "))
Xc = int(input("Xc? "))
Yc = int(input("Yc? "))

AB_slope = ((Ya-Yb)/(Xa-Xb))
BC_slope = ((Yb-Yc)/(Xb-Xc))

Alt1_slope = -1 * (AB_slope ** -1)
Alt2_slope = -1 * (BC_slope ** -1)

x = Symbol('x')
Xo = solve((Alt1_slope*(x-Xc)+Yc)-(Alt2_slope*(x-Xa)+Ya),x)[0]

y = Symbol('y')
Yo = Alt1_slope*(Xo-Xc)+Yc

print("Xo: " + str(Xo))
print("Yo: " + str(Yo))
